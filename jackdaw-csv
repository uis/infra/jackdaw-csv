#! /usr/bin/python3

from argparse import ArgumentParser
import csv
from http.cookiejar import MozillaCookieJar
from io import StringIO
import requests
from sys import stdin, stdout

class JackdawTSV(csv.Dialect):
    delimiter = str("\t")
    quoting = csv.QUOTE_NONE
    strict = True
    lineterminator = "\r\n"

def convert_file(src, dst, header = True):
    r = csv.reader(src, dialect=JackdawTSV)
    if not header: next(r)
    w = csv.writer(dst)
    w.writerows(r)

def fetch_from_jackdaw(**params):
    r = s.get("https://jackdaw.cam.ac.uk/ipreg/xlist_ops",
              params = { "record_separator": "CRLF",
                         "do_it": "list_domain",
                         **params })
    r.raise_for_status()
    if r.headers['Content-Type'] != 'application/octet-stream; charset=utf-8':
        exit(f"surprise content-type: {r.headers['Content-Type']}")
    return r.text

s = requests.Session()
s.headers["User-Agent"] = "jackdaw-csv"
parser = ArgumentParser(epilog="Note that cookie files must start with "
                        "\"# HTTP Cookie File\", which ones from "
                        "Jackdaw don't.")
parser.add_argument("--cookies",
                    help="Mozilla cookie file containing Jackdaw creds")
parser.add_argument("--prefix", default="RA_",
                    help="View prefix to use (default 'RA_')")
cfg = parser.parse_args()

if cfg.cookies != None: s.cookies = MozillaCookieJar(cfg.cookies)
s.cookies.load()

for object_type in [ "box", "vbox", "cname", "aname"]:
    with open(f"{object_type}.csv", 'w') as outfile:
        emit_header = True
        for domain in [ "csi.cam.ac.uk", "csi.private.cam.ac.uk",
                        "csx.cam.ac.uk", "csx.private.cam.ac.uk" ]:
            tsv = fetch_from_jackdaw(prefix=cfg.prefix, object_type=object_type,
                                     domain=domain)
            convert_file(StringIO(tsv, newline=''), outfile, emit_header)
            emit_header = False
